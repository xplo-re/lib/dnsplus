// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// IPv6Address test case.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>
#import <XCTest/XCTest.h>

@interface IP6Address_Test : XCTestCase

@end

@implementation IP6Address_Test

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testInitWithString
{
    IPAddress *generic;
    IP6Address *address;

    NSDictionary *ips = @{// Standard IPs.
                          @"::1": @"::1",
                          @"2001::bffe:1280": @"2001::bffe:1280",
                          @"2002::": @"2002::",
                          @"::": @"::",
                          @"2001:5::3:0:0:0:2": @"2001:5:0:3::2",
                          // Extra zeroes.
                          @"2001:0045:0001:0005:0ffe:0020:0000:0001": @"2001:45:1:5:ffe:20::1",
                          // Embedded IPv4.
                          @"::ffff:10.12.182.1": @"::ffff:10.12.182.1",
                          @"::192.145.1.2": @"::192.145.1.2",
                          };

    // Standard parsing.
    for (NSString *key in ips) {
        generic = [IPAddress addressWithString:key];
        address = [IP6Address addressWithString:key];

        XCTAssertNotNil(generic, @"IP %@ via generic init", key);
        XCTAssertNotNil(address, @"IP %@ via IPv6 init", key);

        if (address) {
            XCTAssertEqualObjects([address class], [IP6Address class], @"IP %@", key);
            XCTAssertEqualObjects(address.description, ips[key], @"IP %@", key);

            if (generic) {
                XCTAssert([address isEqualToIP:generic], @"IP %@", key);
                XCTAssert([generic isEqualToIP:address], @"IP %@", key);
            }
        }
    }
}

- (void)testComparison
{
    IP6Address *a1 = [[IP6Address alloc] initWithWords:2001, 0xebbf, 0, 0x13, 0xbe, 0x1234, 1];
    IP6Address *a2 = [[IP6Address alloc] initWithWords:2001, 0xebbf, 0, 0x13, 0xbe, 0x1234, 2];
    IP6Address *a3 = [[IP6Address alloc] initWithWords:2001, 0xebff, 0, 0x13, 0xbe, 0x1234, 0];
    IP6Address *a4 = [[IP6Address alloc] initWithWords:2001, 0xebbf, 0, 0x13, 0xbe, 0x1234, 1];
    IP6Address *a5 = [[IP6Address alloc] initWithWords:0, 0, 0, 0, 0, 0, 0x7f00, 0x0001];
    IP4Address *a6 = [a4 v4Address];

    XCTAssertNotEqualObjects(a1, a4);
    XCTAssertEqual(NSOrderedSame, [a1 compare:a4]);
    XCTAssertEqual(NSOrderedSame, [a4 compare:a1]);
    XCTAssert([a1 isEqualToIP:a4]);
    XCTAssert([a4 isEqualToIP:a1]);

    XCTAssertEqual(NSOrderedAscending, [a1 compare:a2]);
    XCTAssertEqual(NSOrderedAscending, [a2 compare:a3]);
    XCTAssert(![a1 isEqualToIP:a2]);
    XCTAssert(![a2 isEqualToIP:a3]);

    XCTAssertEqual(NSOrderedDescending, [a3 compare:a4]);
    XCTAssertEqual(NSOrderedDescending, [a3 compare:a2]);

    XCTAssertNotEqualObjects(a5, a6);
    XCTAssert(![a5 isEqualToIP:a6]);
    XCTAssert(![a6 isEqualToIP:a5]);

    XCTAssertNoThrow([a5 compare:a6]);
    XCTAssertNoThrow([a6 compare:a5]);
}

- (void)testDescription
{
    IP6Address *address;

    // Some IPv6 address with zeroed segments.
    address = [[IP6Address alloc] initWithWords:0x2001, 0xfe12, 0x22, 0, 0, 0, 0, 0xbef4];

    XCTAssertNotNil(address);
    XCTAssertEqualObjects(address.longDescription, @"2001:fe12:0022:0000:0000:0000:0000:bef4");
    XCTAssertEqualObjects(address.description, @"2001:fe12:22::bef4");

    // IPv6 address with leading zeroes.
    address = [[IP6Address alloc] initWithWords:0, 0, 0, 0, 0, 0, 0, 0x1];

    XCTAssertNotNil(address);
    XCTAssertEqualObjects(address.longDescription, @"0000:0000:0000:0000:0000:0000:0000:0001");
    XCTAssertEqualObjects(address.description, @"::1");

    // IPv6 address with trailing zeroes.
    address = [[IP6Address alloc] initWithWords:0xffff, 0, 0, 0, 0, 0, 0, 0];

    XCTAssertNotNil(address);
    XCTAssertEqualObjects(address.longDescription, @"ffff:0000:0000:0000:0000:0000:0000:0000");
    XCTAssertEqualObjects(address.description, @"ffff::");

    // Compatible IPv4 address.
    address = [[IP6Address alloc] initWithWords:0, 0, 0, 0, 0, 0, 0xa5bf, 0xfe01];

    XCTAssertNotNil(address);
    XCTAssertEqualObjects(address.longDescription, @"0000:0000:0000:0000:0000:0000:a5bf:fe01");
    XCTAssertEqualObjects(address.description, @"::165.191.254.1");

    // Embedded IPv4 address.
    address = [[IP6Address alloc] initWithWords:0, 0, 0, 0, 0, 0xffff, 0xa5bf, 0xfe01];

    XCTAssertNotNil(address);
    XCTAssertEqualObjects(address.longDescription, @"0000:0000:0000:0000:0000:ffff:a5bf:fe01");
    XCTAssertEqualObjects(address.description, @"::ffff:165.191.254.1");
}

- (void)testV4Conversion
{
    IPAddress *address;

    // Valid IPv4 address (compatible).
    address = [[[IP6Address alloc] initWithWords:0, 0, 0, 0, 0, 0, 0xa5bf, 0xfe01] v4Address];

    XCTAssertNotNil(address);
    XCTAssertTrue(4 == address.version);
    XCTAssertEqualObjects(address.description, @"165.191.254.1");

    // Valid IPv4 address (embedded).
    address = [[[IP6Address alloc] initWithWords:0, 0, 0, 0, 0, 0xffff, 0xa5bf, 0xfe01] v4Address];

    XCTAssertNotNil(address);
    XCTAssertTrue(4 == address.version);
    XCTAssertEqualObjects(address.description, @"165.191.254.1");

    // IPv6 addresses that cannot be converted to an IPv4 address.
    address = [[[IP6Address alloc] initWithWords:0, 0x100, 0, 0, 0, 0xffff, 0xa5bf, 0xfe01] v4Address];
    XCTAssertNil(address);
    address = [[[IP6Address alloc] initWithWords:0, 0, 0, 0, 0, 0, 0, 0] v4Address];
    XCTAssertNil(address);
    address = [[[IP6Address alloc] initWithWords:0, 0, 0, 0, 0, 0, 0, 1] v4Address];
    XCTAssertNil(address);
}

@end
