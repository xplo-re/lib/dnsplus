// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNSServiceRecord test case.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>
#import <XCTest/XCTest.h>

@interface DNSServiceRecord_Test : XCTestCase

@end

@implementation DNSServiceRecord_Test

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testResolveService
{
    // Expected entry:
    // _minecraft._tcp.walhall.eden-galaxy.net. 3 IN SRV 0 0 25565 walhall.eden-galaxy.net.
    DNSHost *host = [DNSHost hostWithName:@"_minecraft._tcp.walhall.eden-galaxy.net"];
    NSArray *result = [host resolve:kDNSServiceRecordType error:NULL];

    XCTAssert(result.count == 1);

    if (result.count >= 1) {
        XCTAssert([result[0] isKindOfClass:[DNSServiceRecord class]]);

        if ([result[0] isKindOfClass:[DNSServiceRecord class]]) {
            DNSServiceRecord *record = result[0];

            XCTAssertEqual(record.port, 25565);
            XCTAssertEqual(record.weight, 0);
            XCTAssertEqual(record.priority, 0);
            XCTAssertEqualObjects(record.target.description, @"walhall.eden-galaxy.net");
        }
    }
}


@end
