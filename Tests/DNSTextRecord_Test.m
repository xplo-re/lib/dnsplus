// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNSServiceRecord test case.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>
#import <XCTest/XCTest.h>

@interface DNSTextRecord_Test : XCTestCase

@end

@implementation DNSTextRecord_Test

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testResolveMultipleStrings
{
    // Public test domain, expected entries:
    // text.rosuav.com.	3	IN	TXT	"Text 1A" "Text 1B"
    // text.rosuav.com.	3	IN	TXT	"Text 2A" "Text 2B"
    DNSHost *host = [DNSHost hostWithName:@"text.rosuav.com"];
    NSArray *result = [host resolve:kDNSTextRecordType error:NULL];

    XCTAssert(result.count == 2);
    XCTAssert([result[0] isKindOfClass:[DNSTextRecord class]]);
    XCTAssert([result[1] isKindOfClass:[DNSTextRecord class]]);

    NSLog(@"%@", result);
}

@end
