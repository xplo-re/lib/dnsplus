// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// IPv4Address test case.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>
#import <XCTest/XCTest.h>

@interface IP4Address_Test : XCTestCase

@end

@implementation IP4Address_Test

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testInitWithString
{
    IPAddress *generic;
    IP4Address *address;

    NSDictionary *ips = @{// Standard IPs.
                          @"127.0.0.1": @"127.0.0.1",
                          @"192.168.0.1": @"192.168.0.1",
                          @"10.12.178.45": @"10.12.178.45",
                          // Extra zeroes.
                          @"001.89.04.002": @"1.89.4.2",
                          // Exotic notations.
                          @"3475821262": @"207.44.210.206",
                          @"0xCF2CD2CE": @"207.44.210.206",
                          @"0xCF.0x2C.0xD2.0xCE": @"207.44.210.206",
                          @"0x11.034114040": @"17.112.152.32",
                          @"0330.0357.045.0145": @"216.239.37.101",
                          };

    // Standard parsing.
    for (NSString *key in ips) {
        generic = [IPAddress addressWithString:key];
        address = [IP4Address addressWithString:key];

        XCTAssertNotNil(generic, @"IP %@ via generic init", key);
        XCTAssertNotNil(address, @"IP %@ via IPv4 init", key);

        if (address) {
            XCTAssertEqualObjects([address class], [IP4Address class], @"IP %@", key);
            XCTAssertEqualObjects(address.description, ips[key], @"IP %@", key);

            if (generic) {
                XCTAssert([address isEqualToIP:generic], @"IP %@", key);
                XCTAssert([generic isEqualToIP:address], @"IP %@", key);
            }
        }
    }
}

- (void)testComparison
{
    IP4Address *a1 = [[IP4Address alloc] initWithOctets:127, 0, 0, 1];
    IP4Address *a2 = [[IP4Address alloc] initWithOctets:127, 0, 0, 2];
    IP4Address *a3 = [[IP4Address alloc] initWithOctets:192, 0, 0, 100];
    IP4Address *a4 = [[IP4Address alloc] initWithOctets:127, 0, 0, 1];
    IP6Address *a6 = [a4 v6Address];

    XCTAssertNotEqualObjects(a1, a4);
    XCTAssertEqual(NSOrderedSame, [a1 compare:a4]);
    XCTAssertEqual(NSOrderedSame, [a4 compare:a1]);
    XCTAssert([a1 isEqualToIP:a4]);
    XCTAssert([a4 isEqualToIP:a1]);

    XCTAssertEqual(NSOrderedAscending, [a1 compare:a2]);
    XCTAssertEqual(NSOrderedAscending, [a2 compare:a3]);
    XCTAssert(![a1 isEqualToIP:a2]);
    XCTAssert(![a2 isEqualToIP:a3]);

    XCTAssertEqual(NSOrderedDescending, [a3 compare:a4]);
    XCTAssertEqual(NSOrderedDescending, [a3 compare:a2]);

    XCTAssertNotEqualObjects(a4, a6);
    XCTAssert(![a4 isEqualToIP:a6]);
    XCTAssert(![a6 isEqualToIP:a4]);

    XCTAssertNoThrow([a4 compare:a6]);
    XCTAssertNoThrow([a6 compare:a4]);
}

- (void)testDescription
{
    IP4Address *address;

    // Typical IPv4 address.
    address = [[IP4Address alloc] initWithOctets:192, 168, 22, 9];

    XCTAssertNotNil(address);
    XCTAssertEqualObjects(address.longDescription, @"192.168.022.009");
    XCTAssertEqualObjects(address.description, @"192.168.22.9");

    // IPv4 address with zero segments and correct conversion of last promoted
    // initialisation parameter from int(-1) to unsigned char(255).
    address = [[IP4Address alloc] initWithOctets:10, 0, 0, -1];

    XCTAssertNotNil(address);
    XCTAssertEqualObjects(address.longDescription, @"010.000.000.255");
    XCTAssertEqualObjects(address.description, @"10.0.0.255");
}

- (void)testV6Conversion
{
    IPAddress *address = [[[IP4Address alloc] initWithOctets:137, 56, 73, 201] v6Address];

    XCTAssertNotNil(address);
    XCTAssertTrue(6 == address.version);
    XCTAssertEqualObjects(address.description, @"::ffff:137.56.73.201");
}

@end
