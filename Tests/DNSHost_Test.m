// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNSHost test case.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>
#import <XCTest/XCTest.h>

@interface DNSHost_Test : XCTestCase

@end

@implementation DNSHost_Test

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testInitInvalid
{
    DNSHost *host;

    host = [DNSHost hostWithName:@"errorneous._._.domain"];
    XCTAssertNil(host);
}

- (void)testInitServiceHostName
{
    DNSHost *host = [DNSHost hostWithDomain:@"local." transport:kDNSUDPProtocol application:@"my-service"];

    XCTAssertNotNil(host);
    XCTAssertEqualObjects(host.name, @"_my-service._udp.local.");
}

- (void)testInitServiceHostNameWithIdent
{
    DNSHost *host = [DNSHost hostWithDomain:@"local." transport:kDNSUDPProtocol application:@"my-service" service:@"Some Device' ID"];

    XCTAssertNotNil(host);
    XCTAssertEqualObjects(host.name, @"Some\\032Device'\\032ID._my-service._udp.local.");
}

- (void)testParseServiceHostName
{
    DNSHost *host = [DNSHost hostWithName:@"_test-service._tcp.local."];

    XCTAssertEqualObjects(host.application, @"test-service");
    XCTAssertEqualObjects(host.transport, kDNSTCPProtocol);
    XCTAssertEqualObjects(host.domain, @"local.");
}

- (void)testParseServiceHostNameWithIdent
{
    DNSHost *host = [DNSHost hostWithName:@"My._test-service._tcp.local."];

    XCTAssertEqualObjects(host.service, @"My");
    XCTAssertEqualObjects(host.application, @"test-service");
    XCTAssertEqualObjects(host.transport, kDNSTCPProtocol);
    XCTAssertEqualObjects(host.domain, @"local.");
}

- (void)testResolveSRV
{
    DNSHost *host = [DNSHost hostWithName: @"_minecraft._tcp.walhall.eden-galaxy.net"];

    NSArray *result = [host resolve:kDNSServiceRecordType error:NULL];

    NSLog(@"%@", result);
}

@end
