// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNSAddressRecord test case.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>
#import <XCTest/XCTest.h>

@interface DNSAddressRecord_Test : XCTestCase

@end

@implementation DNSAddressRecord_Test

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testResolveAddress
{
    // Expected entries:
    // ntp.xplo-re.net.	3	IN	A	94.23.99.154
    // ntp.xplo-re.net.	3	IN	A	94.23.99.153
    // ntp.xplo-re.net.	3	IN	A	94.23.99.155
    DNSHost *host = [DNSHost hostWithName:@"ntp.xplo-re.net"];
    NSArray *result = [host resolve:kDNSAddressRecordType error:NULL];

    XCTAssert(result.count == 3);

    for (DNSAddressRecord *record in result) {
        XCTAssert([record isKindOfClass:[DNSAddressRecord class]]);
        XCTAssert([record.address.description isEqualToString:@"94.23.99.153"] ||
                  [record.address.description isEqualToString:@"94.23.99.154"] ||
                  [record.address.description isEqualToString:@"94.23.99.155"]);
    }
}


@end
