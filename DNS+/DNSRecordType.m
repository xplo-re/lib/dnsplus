// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNS record types, classes and protocols header.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>

#pragma mark Record Types

NSString * const kDNSAddressRecordType = @"A";
NSString * const kDNSNameServerRecordType = @"NS";
NSString * const kDNSCanonicalNameRecordType = @"CNAME";
NSString * const kDNSStartOfAuthorityRecordType = @"SOA";
NSString * const kDNSPointerRecordType = @"PTR";
NSString * const kDNSMailExchangeRecordType = @"MX";
NSString * const kDNSTextRecordType = @"TXT";
NSString * const kDNSAddressV6RecordType = @"AAAA";
NSString * const kDNSLocationRecordType = @"LOC";
NSString * const kDNSServiceRecordType = @"SRV";
NSString * const kDNSSenderPolicyFrameworkRecordType = @"SPF";
NSString * const kDNSIncrementalZoneTransferType = @"IXFR";
NSString * const kDNSAuthoritativeZoneTransferRecordType = @"AXFR";
NSString * const kDNSAnyRecordType = @"ANY";

#pragma mark DNS Classes

NSString * const kDNSInternetClassName = @"IN";
NSString * const kDNSChaosClassName = @"CH";
NSString * const kDNSHesiodClassName = @"HS";

#pragma mark Transport Protocols

NSString * const kDNSTCPProtocol = @"tcp";
NSString * const kDNSUDPProtocol = @"udp";