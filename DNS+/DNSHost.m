// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNS host interface.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>

#pragma mark DNS Host Callback Wrapper

@interface DNSHostCallbackWrapper : NSObject

@property (nonatomic, readonly) DNSHostResolveCallback callback;
@property (nonatomic, readonly) BOOL finished;
@property (nonatomic, readonly) DNSHost *host;
@property (nonatomic, readonly) uint16_t type;

- (id)initWithHost:(DNSHost *)host type:(uint16_t)type callback:(DNSHostResolveCallback)callback;
- (void)processRecord:(NSData *)data flags:(DNSServiceFlags)flags;
- (DNSServiceErrorType)start;
- (void)stopWithError:(DNSServiceErrorType)error;

@end

static void DNSHostQueryCallback(DNSServiceRef serviceRef, DNSServiceFlags flags, uint32_t interfaceIndex, DNSServiceErrorType errorCode, const char *fqdn, uint16_t rrtype, uint16_t rrclass, uint16_t rdlen, const void *rdata, uint32_t ttl, void *context);

@implementation DNSHostCallbackWrapper {
    DNSServiceRef _serviceRef;
    dispatch_queue_t _resolverQueue;
    dispatch_source_t _socketReadSource;
}

- (id)initWithHost:(DNSHost *)host type:(uint16_t)type callback:(DNSHostResolveCallback)callback
{
    self = [super init];

    if (nil != self) {
        _callback = callback;
        _host = host;
        _type = type;
        _finished = NO;

        _resolverQueue = dispatch_queue_create("org.xplo-re.dns-resolver", DISPATCH_QUEUE_SERIAL);
    }

    return self;
}

- (void)processRecord:(NSData *)data flags:(DNSServiceFlags)flags;
{
    id record = [DNSRecord recordWithHost:_host data:data];

    if (nil != record) {
        _callback(record, flags & kDNSServiceFlagsMoreComing);
    }
    else {
        // TODO
    }
}

- (DNSServiceErrorType)start
{
    DNSServiceErrorType serviceError;
    const char *hostName = _host.name.UTF8String;

    if (NULL == hostName) {
        return kDNSServiceErr_BadParam;
    }
    NSLog(@"starting for type %i", _type);
    serviceError =
        DNSServiceQueryRecord(&_serviceRef, kDNSServiceFlagsReturnIntermediates, kDNSServiceInterfaceIndexAny, hostName, _type, kDNSServiceClass_IN, DNSHostQueryCallback, (__bridge void *)self);

    if (kDNSServiceErr_NoError == serviceError) {
        int fd = DNSServiceRefSockFD(_serviceRef);

        _finished = NO;

        _socketReadSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_READ, fd, 0, _resolverQueue);

        dispatch_source_set_event_handler(_socketReadSource, ^(void) {
            NSLog(@"dispatch read event, processing result");
            DNSServiceErrorType serviceError;
            serviceError = DNSServiceProcessResult(_serviceRef);

            if (kDNSServiceErr_NoError != serviceError) {
                [self stopWithError:serviceError];
            }
        });

        dispatch_resume(_socketReadSource);
    }

    if (kDNSServiceErr_NoError != serviceError) {
        [self stopWithError:serviceError];
    }
    
    return serviceError;
}

- (void)stopWithError:(DNSServiceErrorType)error
{
    if (_serviceRef) {
        DNSServiceRefDeallocate(_serviceRef);
        _serviceRef = NULL;
    }

    _finished = YES;
}

#pragma mark -
#pragma mark Callbacks

// Called via DNSServiceProcessResult as service callback when a response for the query arrived (which is invoked by GCD).
static void DNSHostQueryCallback(DNSServiceRef serviceRef, DNSServiceFlags flags, uint32_t interfaceIndex, DNSServiceErrorType errorCode, const char *fqdn, uint16_t rrtype, uint16_t rrclass, uint16_t rdlen, const void *rdata, uint32_t ttl, void *context)
{
    NSLog(@"%s", __PRETTY_FUNCTION__);

    DNSHostCallbackWrapper *wrapper = (__bridge DNSHostCallbackWrapper *)context;

    NSCAssert([wrapper isKindOfClass:[DNSHostCallbackWrapper class]], @"Context not kind of wrapper");
    NSCAssert(serviceRef == wrapper->_serviceRef, @"Service reference does not match");
    NSCAssert(flags & kDNSServiceFlagsAdd, @"Unsupported service flags");
    NSCAssert(rrtype == wrapper->_type, @"Record type does not match");
    NSCAssert(rrclass == kDNSServiceClass_IN, @"DNS class does not match");

    if (errorCode == kDNSServiceErr_NoError) {
        // Build a full resource record header and let dns_parse_resource_record()
        // (used by the wrapper) take care of parsing the data.
        // Cf. RFC 6895 for details: http://tools.ietf.org/html/rfc6895
        uint8_t u8;
        uint16_t encodedLength = htons(rdlen);
        NSMutableData *record = [NSMutableData dataWithCapacity:rdlen + sizeof(u8) + sizeof(rrtype) + sizeof(rrclass) + sizeof(ttl) + sizeof(encodedLength)];

        // Owner name, label. Unused, hence fixed to NUL.
        u8 = 0;
        [record appendBytes:&u8 length:sizeof(u8)];

        // Type, 2-octet unsigned integer containing one of the RRTYPE codes.
        rrtype = htons(rrtype);
        [record appendBytes:&rrtype length:sizeof(rrtype)];

        // DNS class, 2-octet unsigned integer containing one of the RR CLASS codes.
        rrclass = htons(rrclass);
        [record appendBytes:&rrclass length:sizeof(rrclass)];

        // Time to live, 4-octet (32-bit) unsigned integer that specifies, for data
        // TYPEs, the number of seconds that the resource record may be cached
        // before the source of the information should again be consulted. Zero
        // is interpreted to mean that the RR can only be used for the
        // transaction in progress.
        ttl = htonl(ttl);
        [record appendBytes:&ttl length:sizeof(ttl)];

        // Data length, unsigned 16-bit integer that specifies the length in
        // octets of the RDATA field.
        [record appendBytes:&encodedLength length:sizeof(encodedLength)];

        // Data; a variable-length string of octets that constitutes the
        // resource.  The format of this information varies according to the
        // type and, in some cases, the DNS class of the resource record.
        [record appendBytes:rdata length:rdlen];

        // Let wrapper parse the record.
        [wrapper processRecord:record flags:flags];

        // Quit request if no more records will be returned. Unicast DNS assumed,
        // more dynamic situations such as multicast DNS or long-lived queries
        // in Back to My Mac) are not covered by this framework.
        if (!(flags & kDNSServiceFlagsMoreComing)) {
            [wrapper stopWithError:kDNSServiceErr_NoError];
        }
    }
    else {
        [wrapper stopWithError:errorCode];
    }
}

@end

#pragma mark -

@implementation DNSHost

#pragma mark Initialisation

- (id)initWithDomain:(NSString *)domain
{
    return [self initWithDomain:domain transport:nil application:nil service:nil];
}

+ (id)hostWithDomain:(NSString *)domain
{
    return [[self alloc] initWithDomain:domain transport:nil application:nil service:nil];
}

- (id)initWithDomain:(NSString *)domain transport:(NSString *)transport application:(NSString *)application
{
    return [self initWithDomain:domain transport:transport application:application service:nil];
}

+ (id)hostWithDomain:(NSString *)domain transport:(NSString *)transport application:(NSString *)application
{
    return [[self alloc] initWithDomain:domain transport:transport application:application service:nil];
}

- (id)initWithDomain:(NSString *)domain transport:(NSString *)transport application:(NSString *)application service:(NSString *)service
{
    if (nil == domain || !domain.length || ((transport != application || service != nil) && (nil == transport || nil == application))) {
        // Domain cannot be null or empty. Service is optional and requires
        // application and transport protocol to be present otherwise.
        // Application and transport protocol cannot be used alone, only together,
        // and are optional.
        return nil;
    }

    self = [super init];

    if (nil != self) {
        if (application) {
            DNSServiceErrorType error;
            char buffer[kDNSServiceMaxDomainName];
            const char *serviceTypeName = [[NSString stringWithFormat:@"_%@._%@", application, transport] UTF8String];

            error = DNSServiceConstructFullName(buffer, [service UTF8String], serviceTypeName, [domain UTF8String]);

            if (error != kDNSServiceErr_NoError) {
                // Bad arguments.
                return nil;
            }
            
            _domain = [domain copy];
            _transport = [transport copy];
            _application = [application copy];
            _service = [service copy];
            _name = [NSString stringWithCString:buffer encoding:NSASCIIStringEncoding];
        }
        else {
            _domain =
            _name = [domain copy];
        }
    }

    return self;
}

+ (id)hostWithDomain:(NSString *)domain transport:(NSString *)transport application:(NSString *)application service:(NSString *)service
{
    return [[self alloc] initWithDomain:domain transport:transport application:application service:service];
}

- (id)initWithName:(NSString *)name
{
    if (nil == name || !name.length) {
        return nil;
    }

    self = [super init];

    if (nil != self) {
        _name = [name copy];

        // Test hostname for service name (optional), service type
        // (application and transport protocols) and domain name parts.
        NSRange result;
        NSRange substringRange;
        NSRange searchRange = NSMakeRange(0, _name.length);
        NSCharacterSet *delimiters = [NSCharacterSet characterSetWithCharactersInString:@"."];
        NSString *segment;
        NSMutableArray *segments = [NSMutableArray arrayWithCapacity:3];

        do {
            segment = nil;

            // Service type segments start with an underscore and all
            // segments end in a dot.
            if (!segments.count || '_' == [_name characterAtIndex:searchRange.location]) {
                result = [_name rangeOfCharacterFromSet:delimiters options:0 range:searchRange];

                if (result.location != NSNotFound) {
                    substringRange = NSMakeRange(searchRange.location, result.location - searchRange.location);
                    segment = [_name substringWithRange:substringRange];

                    searchRange.location = NSMaxRange(result);
                    searchRange.length = _name.length - searchRange.location;

                    if (segment.length > (segments.count ? 1 : 0)) {
                        [segments addObject:segment];
                    }
                    else {
                        // Malformed hostname.
                        return nil;
                    }
                }
            }
        }
        while (segment && segments.count < 3);

        if (segments.count == 2) {
            // Extracted service type only, no service name. Both must start
            // with an underscore.
            if ('_' == [segments[0] characterAtIndex:0]) {
                _application = [segments[0] substringWithRange:NSMakeRange(1, [segments[0] length] - 1)];
                _transport = [segments[1] substringWithRange:NSMakeRange(1, [segments[1] length] - 1)];
            }
        }
        else if (segments.count == 3) {
            // All service components found.
            _service = segments[0];
            _application = [segments[1] substringWithRange:NSMakeRange(1, [segments[1] length] - 1)];
            _transport = [segments[2] substringWithRange:NSMakeRange(1, [segments[2] length] - 1)];
        }

        if (_application) {
            // Leftover domain part is the domain name.
            _domain = [name substringWithRange:searchRange];
        }
        else {
            // Service detection failed, domain equals hostname.
            _domain = _name;
        }
    }

    return self;
}

+ (id)hostWithName:(NSString *)name
{
    return [[self alloc] initWithName:name];
}

#pragma mark Resolver

- (NSArray *)resolve:(NSString *)type error:(NSError **)error
{
    __block NSMutableArray *found = [NSMutableArray array];
    __block BOOL finished = NO;

    [self resolve:type error:error callback:^void(DNSRecord *record, BOOL more){
        [found addObject:record];

        finished = !more;
    }];

    while (!finished) {
        usleep(200);
    }

    return [found copy];
}

- (BOOL)resolve:(NSString *)type error:(NSError **)error callback:(DNSHostResolveCallback)callback
{
    Class recordClass = [DNSRecord classForType:type];
    DNSHostCallbackWrapper *wrapper = [[DNSHostCallbackWrapper alloc] initWithHost:self type:[recordClass typeID] callback:callback];
    DNSServiceErrorType serviceError;

    serviceError = [wrapper start];

    if (kDNSServiceErr_NoError == serviceError) {
        return YES;
    }
    else {
        if (nil != error) {
            // TODO: Format error
        }

        return NO;
    }
}

@end
