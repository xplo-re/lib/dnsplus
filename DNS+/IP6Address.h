// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// IPv6 address.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/IPAddress.h>

#pragma mark Support Structures

typedef struct _IP6AddressData {
    union {
        uint8_t u8[16];
        uint16_t u16[8];
        uint32_t u32[4];
    };
} IP6AddressData;

#pragma mark -

@interface IP6Address : IPAddress

#pragma mark Initialisation

/**
 * Initialises an IPv6 address with 8 provided 16-bit words.
 * @param word0
 * First of 8 expected address words in host byte order.
 */
- (id)initWithWords:(unsigned)word0, ...;

// Predefined multicast addresses:
// https://www.iana.org/assignments/ipv6-multicast-addresses/ipv6-multicast-addresses.xhtml

/**
 * Returns the node-local scope multicast address targeting all nodes.
 * @ref http://www.iana.org/go/rfc4291
 */
+ (id)nodeLocalNodesAddress;
/**
 * Returns the node-local scope multicast address targeting all routers.
 * @ref http://www.iana.org/go/rfc4291
 */
+ (id)nodeLocalRoutersAddress;

/**
 * Returns the link-local scope multicast address targeting all nodes.
 * @ref http://www.iana.org/go/rfc4291
 */
+ (id)linkLocalNodesAddress;
/**
 * Returns the link-local scope multicast address targeting all routers.
 * @ref http://www.iana.org/go/rfc4291
 */
+ (id)linkLocalRoutersAddress;
/**
 * Returns the link-local scope multicast address targeting all MLDv2-capable routers.
 * @ref http://www.iana.org/go/rfc3810
 */
+ (id)linkLocalMLDv2RoutersAddress;
/**
 * Returns the link-local scope multicast address targeting all DHCP agents.
 * @ref http://www.iana.org/go/rfc3315
 */
+ (id)linkLocalDHCPAgentsAddress;

/**
 * Returns the site-local scope multicast address targeting all routers.
 * @ref http://www.iana.org/go/rfc4291
 */
+ (id)siteLocalRoutersAddress;
/**
 * Returns the site-local scope multicast address targeting all DHCP servers.
 * @ref http://www.iana.org/go/rfc3315
 */
+ (id)siteLocalDHCPServersAddress;

#pragma mark Properties

/**
 * @p YES, if address is link-local, otherwise @p NO.
 */
@property (nonatomic, readonly, getter=isLinkLocal) BOOL linkLocal;
/**
 * @p YES, if address is the loopback address ::1, otherwise @p NO.
 */
@property (nonatomic, readonly, getter=isLoopback) BOOL loopback;
/**
 * @p YES, if address is a multicast target, otherwise @p NO.
 */
@property (nonatomic, readonly, getter=isMulticast) BOOL multicast;
/**
 * @p YES, if address is site-local, otherwise @p NO.
 */
@property (nonatomic, readonly, getter=isSiteLocal) BOOL siteLocal;
/**
 * @p YES, if address is local and unique, otherwise @p NO.
 */
@property (nonatomic, readonly, getter=isUniqueLocal) BOOL uniqueLocal;
/**
 * @c YES, if address is the unspecified address ::, otherwise @c NO.
 */
@property (nonatomic, readonly, getter=isUnspecified) BOOL unspecified;
/**
 * Indicates, whether the IPv6 address is an IPv4 compatible address in the
 * form ::1.2.3.4.
 */
@property (nonatomic, readonly, getter=isV4Compatible) BOOL v4Compatible;
/**
 * Indicates, whether the IPv6 address represents an embedded IPv4 address in
 * the form ::ffff:1.2.3.4.
 */
@property (nonatomic, readonly, getter=isV4Mapped) BOOL v4Mapped;

@end
