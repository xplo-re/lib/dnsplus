// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// IP address.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <Foundation/Foundation.h>

@class IP4Address;
@class IP6Address;

@interface IPAddress : NSObject

#pragma mark Initialisation

/**
 * Initialises an IP address from binary data. The type of the IP address is
 * automatically determined by the length of the data.
 * @param data
 * Binary data of given @a length.
 * @param length
 * Length in octets of binary @a data. Can be 0.
 *
 * A length of 4 or below will yield an IPv4 address, padding trailing octets
 * for a length below 4 with zeroes.
 * Otherwise an IPv6 address is initialised, padding trailing octets for a
 * length below 16 with zeroes.
 */
- (id)initWithData:(void *)data length:(NSUInteger)length;
/**
 * Allocates and initialises an IP address from binary data. The type of the
 * IP address is automatically determined by the length of the data.
 * @param data
 * Binary data of given @a length.
 * @param length
 * Length in octets of binary @a data. Can be 0.
 *
 * A length of 4 or below will yield an IPv4 address, padding trailing octets
 * for a length below 4 with zeroes.
 * Otherwise an IPv6 address is initialised, padding trailing octets for a
 * length below 16 with zeroes.
 */
+ (id)addressWithData:(void *)data length:(NSUInteger)length;
/**
 * Initialises an IP address from a given @a address string. If the string
 * contains a valid IPv4 or IPv6 address, an IP4Address or IP6Address
 * instance is returned, respectively, in that order. Otherwise @p nil is
 * returned.
 * @param address
 * IP address string to parse.
 */
- (id)initWithString:(NSString *)address;
/**
 * Allocates and initialises an IP address from a given @a address string. If
 * the string contains a valid IPv4 or IPv6 address, an IP4Address or IP6Address
 * instance is returned, respectively, in that order. Otherwise @p nil is
 * returned.
 * @param address
 * IP address string to parse.
 */
+ (id)addressWithString:(NSString *)address;

/**
 * Allocates and initialises the corresponding loopback address of the
 * specific IP version subclass (127.0.0.1 in case of IPv4, ::1 in case of IPv6).
 * If no loopback address exists for the corresponding subclass, @p nil is returned
 * instead.
 */
+ (id)loopbackAddress;
/**
 * Allocates and initialises the corresponding undefined address of the
 * specific IP version subclass (0.0.0.0 in case of IPv4, :: in case of IPv6).
 */
+ (id)unspecifiedAddress;

#pragma mark Conversion

/**
 * Full width string repesentation of IP address.
 */
@property (nonatomic, readonly) NSString *longDescription;
/**
 * IP address converted to an IPv4 address. If conversion is not possible,
 * @p nil is returned instead.
 */
@property (nonatomic, readonly) IP4Address *v4Address;
/**
 * IP address converted to an IPv6 address.
 */
@property (nonatomic, readonly) IP6Address *v6Address;

#pragma mark Properties

/**
 * Pointer to internal corresponding in_addr structure of receiver.
 */
@property (nonatomic, readonly) const void *in_addr;
/**
 * IP version number: 4 for IPv4, 6 for IPv6.
 */
@property (nonatomic, readonly) NSUInteger version;

#pragma mark Comparison

- (NSComparisonResult)compare:(IPAddress *)address;
- (BOOL)isEqualToIP:(IPAddress *)address;

@end
