// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNS record types, classes and protocols header.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#ifndef DNS_RECORD_TYPE_H__
#define DNS_RECORD_TYPE_H__

#pragma mark Record Types

FOUNDATION_EXPORT NSString * const kDNSAddressRecordType;
FOUNDATION_EXPORT NSString * const kDNSNameServerRecordType;
FOUNDATION_EXPORT NSString * const kDNSCanonicalNameRecordType;
FOUNDATION_EXPORT NSString * const kDNSStartOfAuthorityRecordType;
FOUNDATION_EXPORT NSString * const kDNSPointerRecordType;
FOUNDATION_EXPORT NSString * const kDNSMailExchangeRecordType;
FOUNDATION_EXPORT NSString * const kDNSTextRecordType;
FOUNDATION_EXPORT NSString * const kDNSAddressV6RecordType;
FOUNDATION_EXPORT NSString * const kDNSLocationRecordType;
FOUNDATION_EXPORT NSString * const kDNSServiceRecordType;
FOUNDATION_EXPORT NSString * const kDNSSenderPolicyFrameworkRecordType;
FOUNDATION_EXPORT NSString * const kDNSIncrementalZoneTransferType;
FOUNDATION_EXPORT NSString * const kDNSAuthoritativeZoneTransferRecordType;
FOUNDATION_EXPORT NSString * const kDNSAnyRecordType;

#pragma mark DNS Classes

typedef enum _DNSClass {
    kDNSInternetClass = 1,
    kDNSChaosClass = 3,
    kDNSHesiodClass = 4,
    kDNSPrivateStartClass = 0xff00,
    kDNSPrivateEndClass = 0xfffe,
} DNSClass;

FOUNDATION_EXPORT NSString * const kDNSInternetClassName;
FOUNDATION_EXPORT NSString * const kDNSChaosClassName;
FOUNDATION_EXPORT NSString * const kDNSHesiodClassName;

#pragma mark Transport Protocols

FOUNDATION_EXPORT NSString * const kDNSTCPProtocol;
FOUNDATION_EXPORT NSString * const kDNSUDPProtocol;

#endif
