// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNS record base interface.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <Foundation/Foundation.h>
#import <DNS+/DNSHost.h>
#import <DNS+/DNSRecordType.h>

@interface DNSRecord : NSObject

@property (assign, readonly) DNSClass DNSClass;
@property (readonly) DNSHost *host;
@property (assign, readonly) NSInteger timeToLive;

#pragma mark Initialisation

- (id)initWithHost:(DNSHost *)host timeToLive:(NSInteger)ttl;

- (id)initWithHost:(DNSHost *)host data:(void *)data;
+ (id)recordWithHost:(DNSHost *)host data:(NSData *)data;

#pragma mark Record Type

/**
 * DNS record type name.
 */
@property (nonatomic, readonly) NSString *type;
/**
 * DNS record type ID as used by the binary DNS query protocol.
 */
@property (nonatomic, readonly) NSUInteger typeID;

/**
 * DNS record type name provided by the class. This is the primary identifier
 * for DNS record types throughout the framework.
 * @remarks
 * Subclasses must override this method and return the proper type name for
 * the implemented DNS record type.
 */
+ (NSString *)type;
/**
 * DNS type ID as used by the binary DNS query protocol.
 * @remarks
 * Subclasses must override this method and return the proper type ID for
 * the implemented DNS record type.
 */
+ (NSUInteger)typeID;

/**
 * Returns the class for a given record @a type.
 * @param type
 * Record type name to retrieve record class for.
 * @return
 * Class for given record type name or @p nil, if no such record type has been
 * registered.
 */
+ (Class)classForType:(NSString *)type;
+ (Class)classForTypeID:(NSUInteger)type;
/**
 * Registers DNS record @a class. The corresponding type is automatically
 * retrieved from the class @a type method. If a record class for the same
 * type is already registered, the additional registration is skipped.
 *
 * Implementations of DNS record types usually auto-register themselves on
 * load using the following code fragment:
 * @code
 * + (void)load
 * {
 *     [self register:self];
 * }
 * @endcode
 * @note
 * Class registration performs integrity checks. If @a class does not provide
 * a type or does not implement an own type differing from @a DNSRecord, an
 * exception is thrown.
 */
+ (void)register:(Class)class;

#pragma mark Reflection

@property (nonatomic, readonly) NSDictionary *properties;

@end
