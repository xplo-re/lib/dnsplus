// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNS text (TXT) record interface.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNSRecord.h>

@interface DNSTextRecord : DNSRecord

@property (nonatomic, readonly) NSString *text;
@property (nonatomic, readonly) NSArray *texts;

#pragma mark Initialisation

- (id)initWithHost:(DNSHost *)host timeToLive:(NSInteger)ttl text:(NSArray *)texts;

@end
