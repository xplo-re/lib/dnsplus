// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNS host interface.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <Foundation/Foundation.h>
#import <DNS+/DNSRecordType.h>

@class DNSRecord;

typedef void (^DNSHostResolveCallback)(DNSRecord *, BOOL);

@interface DNSHost : NSObject

@property (nonatomic, readonly) NSString *name;

#pragma mark Segments

/// Symbolic name of the service (application protocol of service type).
@property (nonatomic, readonly) NSString *application;
/// Service domain name.
@property (nonatomic, readonly) NSString *domain;
/// Service instance name (optional, mDNS/DNS-SD only).
@property (nonatomic, readonly) NSString *service;
/// Transport protocol of the service.
@property (nonatomic, readonly) NSString *transport;

#pragma mark Initialisation

- (id)initWithDomain:(NSString *)domain;
+ (id)hostWithDomain:(NSString *)domain;
- (id)initWithDomain:(NSString *)domain transport:(NSString *)transport application:(NSString *)application;
+ (id)hostWithDomain:(NSString *)domain transport:(NSString *)transport application:(NSString *)application;
- (id)initWithDomain:(NSString *)domain transport:(NSString *)transport application:(NSString *)application service:(NSString *)service;
+ (id)hostWithDomain:(NSString *)domain transport:(NSString *)transport application:(NSString *)application service:(NSString *)service;
- (id)initWithName:(NSString *)name;
+ (id)hostWithName:(NSString *)name;

#pragma mark Resolver

- (NSArray *)resolve:(NSString *)type error:(NSError **)error;
- (BOOL)resolve:(NSString *)type error:(NSError **)error callback:(DNSHostResolveCallback)callback;

@end
