// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNS service (SRV) record interface.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNSRecord.h>

@interface DNSServiceRecord : DNSRecord

@property (assign, readonly) NSInteger port;
@property (assign, readonly) NSInteger priority;
@property (readonly) NSString *target;
@property (assign, readonly) NSInteger weight;

#pragma mark Initialisation

- (id)initWithHost:(DNSHost *)host timeToLive:(NSInteger)ttl priority:(NSInteger)priority weight:(NSInteger)weight port:(NSInteger)port target:(NSString *)target;

@end
