// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNS address (A) record interface.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNSRecord.h>

@interface DNSAddressRecord : DNSRecord

@property (readonly) IPAddress *address;

#pragma mark Initialisation

- (id)initWithHost:(DNSHost *)host timeToLive:(NSInteger)ttl address:(IPAddress *)address;

@end
