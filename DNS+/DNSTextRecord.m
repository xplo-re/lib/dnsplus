// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNS text (TXT) record interface.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>

@implementation DNSTextRecord

#pragma mark Record Type

+ (void)load
{
    [self register:self];
}

#pragma mark Initialisation

- (id)initWithHost:(DNSHost *)host timeToLive:(NSInteger)ttl text:(NSArray *)texts;
{
    self = [super initWithHost:host timeToLive:ttl];

    if (nil != self) {
        _texts = texts;
    }

    return self;
}

- (id)initWithHost:(DNSHost *)host data:(void *)data
{
    self = [super initWithHost:host data:data];

    if (nil != self) {
        dns_resource_record_t *rr = (dns_resource_record_t *)data;
        NSMutableArray *texts = [NSMutableArray arrayWithCapacity:rr->data.TXT->string_count];

        for (NSUInteger i = 0; i < rr->data.TXT->string_count; i += 1) {
            [texts addObject:[NSString stringWithCString:rr->data.TXT->strings[i] encoding:NSUTF8StringEncoding]];
        }

        _texts = [texts copy];
    }

    return self;
}

#pragma mark Description

- (NSString *)description
{
    NSMutableArray *strings = [NSMutableArray arrayWithCapacity:self.texts.count];

    for (NSString *text in self.texts) {
        [strings addObject:[text stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    }

    return [NSString stringWithFormat:@"%@ \"%@\"", [super description], [strings componentsJoinedByString:@"\" \""]];
}

#pragma mark Record Type

+ (NSString *)type
{
    return kDNSTextRecordType;
}

+ (NSUInteger)typeID
{
    return kDNSServiceType_TXT;
}

#pragma mark Properties

- (NSString *)text
{
    return [self.texts componentsJoinedByString:@" "];
}

#pragma mark Reflection

- (NSDictionary *)properties
{
    NSMutableDictionary *properties = [NSMutableDictionary dictionaryWithDictionary:super.properties];

    properties[@"texts"] = self.texts;

    return [properties copy];
}

@end
