// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// Full framework header.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#pragma mark Network Support

#import <DNS+/IPAddress.h>
#import <DNS+/IP4Address.h>
#import <DNS+/IP6Address.h>

#pragma mark DNS Record Types

#import <DNS+/DNSRecord.h>

#import <DNS+/DNSAddressRecord.h>
#import <DNS+/DNSAddressV6Record.h>
#import <DNS+/DNSServiceRecord.h>
#import <DNS+/DNSTextRecord.h>

#pragma mark DNS Resolver

#import <DNS+/DNSHost.h>