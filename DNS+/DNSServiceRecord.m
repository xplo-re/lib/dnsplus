// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNS service (SRV) record interface.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>

@implementation DNSServiceRecord

#pragma mark Record Type

+ (void)load
{
    [self register:self];
}

#pragma mark Initialisation

- (id)init
{
    return [self initWithHost:nil timeToLive:-1 priority:-1 weight:-1 port:-1 target:nil];
}

- (id)initWithHost:(DNSHost *)host timeToLive:(NSInteger)ttl
{
    return [self initWithHost:host timeToLive:ttl priority:-1 weight:-1 port:-1 target:nil];
}

- (id)initWithHost:(DNSHost *)host timeToLive:(NSInteger)ttl priority:(NSInteger)priority weight:(NSInteger)weight port:(NSInteger)port target:(NSString *)target
{
    self = [super initWithHost:host timeToLive:ttl];
    
    if (nil != self) {
        _priority = priority;
        _weight = weight;
        _port = port;
        _target = target;
    }

    return self;
}

- (id)initWithHost:(DNSHost *)host data:(void *)data
{
    self = [super initWithHost:host data:data];

    if (nil != self) {
        dns_resource_record_t *rr = (dns_resource_record_t *)data;

        _priority = rr->data.SRV->priority;
        _weight = rr->data.SRV->weight;
        _port = rr->data.SRV->port;
        _target = [NSString stringWithCString:rr->data.SRV->target encoding:NSASCIIStringEncoding];
    }

    return self;
}

#pragma mark Description

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ %li %li %li %@.", [super description], self.priority, self.weight, self.port, self.target];
}

#pragma mark Record Type

+ (NSString *)type
{
    return kDNSServiceRecordType;
}

+ (NSUInteger)typeID
{
    return kDNSServiceType_SRV;
}

#pragma mark Reflection

- (NSDictionary *)properties
{
    NSMutableDictionary *properties = [NSMutableDictionary dictionaryWithDictionary:super.properties];

    properties[@"priority"] = [NSNumber numberWithInteger:self.priority];
    properties[@"weight"] = [NSNumber numberWithInteger:self.weight];
    properties[@"port"] = [NSNumber numberWithInteger:self.port];
    properties[@"target"] = self.target;

    return [properties copy];
}

@end
