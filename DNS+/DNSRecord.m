// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNS record base interface.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>

@implementation DNSRecord

#pragma mark Initialisation

- (id)init
{
    return [self initWithHost:nil timeToLive:-1];
}

- (id)initWithHost:(DNSHost *)host timeToLive:(NSInteger)ttl
{
    if (nil == host) {
        return nil;
    }

    self = [super init];

    if (nil != self) {
        // This is the only currently supported DNS class.
        _DNSClass = kDNSInternetClass;

        _host = host;
        _timeToLive = ttl;
    }

    return self;
}

- (id)initWithHost:(DNSHost *)host data:(void *)data
{
    dns_resource_record_t *rr = (dns_resource_record_t *)data;

    return [self initWithHost:host timeToLive:rr->ttl];
}

+ (id)recordWithHost:(DNSHost *)host data:(NSData *)data
{
    id record;
    Class class;
    dns_resource_record_t *rr;

    if (nil == data || data.length > UINT32_MAX) {
        return nil;
    }

    // Use system DNS library to parse the record. This will even succeed for
    // records that are not natively supported (data will be raw), but we will
    // gain access to the header fields that contain the type, DNS class and
    // TTL.
    rr = dns_parse_resource_record(data.bytes, (uint32_t)data.length);

    if (!rr) {
        return nil;
    }

    // Use type ID from header to derive the correct record class.
    class = [self classForTypeID:rr->dnstype];

    if (!class) {
        return nil;
    }

    record = [[class alloc] initWithHost:host data:rr];

    // Cleanup.
    dns_free_resource_record(rr);

    return record;
}

#pragma mark Record Type

NSMutableDictionary *types;

- (NSString *)type
{
    return [[self class] type];
}

- (NSUInteger)typeID
{
    return [[self class] typeID];
}

+ (NSString *)type
{
    return kDNSAnyRecordType;
}

+ (NSUInteger)typeID
{
    return kDNSServiceType_ANY;
}

+ (void)load
{
    types = [NSMutableDictionary dictionary];
}

+ (Class)classForType:(NSString *)type
{
    return types[type];
}

+ (Class)classForTypeID:(NSUInteger)type
{
    for (NSString *key in types) {
        Class class = types[key];

        if (class.typeID == type) {
            return class;
        }
    }

    return nil;
}

+ (void)register:(Class)class
{
    NSAssert([class isSubclassOfClass:[DNSRecord class]], @"DNS record registration of class %@ that is not a subclass of DNSRecord", class);

    NSString *type = [class type];

    NSAssert(type != nil, @"DNS record registration of class %@ that has no type", class);
    NSAssert(![type isEqualTo:kDNSAnyRecordType], @"DNS record registration of class %@ that has type ANY", class);

    if (nil != types[type]) {
        NSLog(@"warning: DNS record type %@ already registered by class %@, skipping registration for class %@", type, types[type], class);
    }
    else {
        types[type] = class;
    }
}

+ (NSDictionary *)types
{
    return [types copy];
}

#pragma mark Description

- (NSString *)description
{
    NSString *className;

    switch (self.DNSClass) {
        case kDNSInternetClass:
            className = kDNSInternetClassName;
            break;
        case kDNSChaosClass:
            className = kDNSChaosClassName;
            break;
        case kDNSHesiodClass:
            className = kDNSHesiodClassName;
            break;
        default:
            className = [NSString stringWithFormat:@"%u", self.DNSClass];
            break;
    }

    return [NSString stringWithFormat:@"%@. %li %@ %@", self.host.name, self.timeToLive, className, self.type];
}

#pragma mark Reflection

- (NSDictionary *)properties
{
    return @{@"dnsClass": [NSNumber numberWithUnsignedInteger:self.DNSClass],
             @"type": self.type,
             @"host": self.host,
             @"timeToLive": [NSNumber numberWithInteger:self.timeToLive]};
}

@end
