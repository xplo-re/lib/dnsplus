// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// IP address.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>

@implementation IPAddress

#pragma mark Initialisation

- (id)initWithData:(void *)data length:(NSUInteger)length
{
    return [[self class] addressWithData:data length:length];
}

+ (id)addressWithData:(void *)data length:(NSUInteger)length
{
    if (length <= 4) {
        return [[IP4Address alloc] initWithData:data length:length];
    }
    else {
        return [[IP6Address alloc] initWithData:data length:length];
    }
}

- (id)initWithString:(NSString *)address
{
    return [[self class] addressWithString:address];
}

+ (id)addressWithString:(NSString *)address
{
    IP6AddressData data;
    const char *addressString = address.UTF8String;

    if (inet_aton(address.UTF8String, (struct in_addr *)&data) > 0) {
        // Conversion succeeded, we have a valid IPv4 string.
        return [[IP4Address alloc] initWithData:&data length:sizeof(IP4AddressData)];
    }

    if (inet_pton(AF_INET6, addressString, &data) > 0) {
        // Conversion succeeded, we have a valid IPv6 string.
        return [[IP6Address alloc] initWithData:&data length:sizeof(IP6AddressData)];
    }

    return nil;
}

+ (id)loopbackAddress
{
    return nil;
}

+ (id)unspecifiedAddress
{
    return [[self alloc] init];
}

#pragma mark Conversion

- (NSString *)longDescription
{
    return nil;
}

- (IP4Address *)v4Address
{
    return nil;
}

- (IP6Address *)v6Address
{
    return nil;
}

#pragma mark Properties

- (const void *)in_addr
{
    return NULL;
}

- (NSUInteger)version
{
    return 0;
}

#pragma mark Comparison

- (NSComparisonResult)compare:(IPAddress *)address
{
    return NSOrderedSame;
}

- (BOOL)isEqualToIP:(IPAddress *)address
{
    if ([[address class] isSubclassOfClass:[self class]]) {
        return (NSOrderedSame == [self compare:address]) ? YES : NO;
    }

    return NO;
}

@end
