// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// IPv4 address.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>

@implementation IP4Address {
    IP4AddressData _data;
}

#pragma mark Initialisation

- (id)initWithData:(void *)data length:(NSUInteger)length
{
    self = [super init];

    if (nil != self) {
        memcpy(&_data, data, MIN(length, sizeof(_data)));
    }

    return self;
}

- (id)initWithOctets:(unsigned)octet0, ...
{
    self = [super init];

    if (nil != self) {
        va_list args;

        va_start(args, octet0);

        _data.u8[0] = octet0;

        for (NSUInteger i = 1; i < 4; i += 1) {
            _data.u8[i] = va_arg(args, unsigned);
        }

        va_end(args);
    }

    return self;
}

- (id)initWithString:(NSString *)address
{
    self = [super init];

    if (nil != self) {
        if (inet_aton(address.UTF8String, (struct in_addr *)&_data) <= 0) {
            return nil;
        }
    }

    return self;
}

+ (id)addressWithString:(NSString *)address
{
    return [[IP4Address alloc] initWithString:address];
}

+ (id)broadcastAddress
{
    return [[self alloc] initWithOctets:0xff, 0xff, 0xff, 0xff];
}

+ (id)loopbackAddress
{
    return [[self alloc] initWithOctets:127, 0, 0, 1];
}

#pragma mark Conversion

- (NSString *)description
{
    char buffer[INET_ADDRSTRLEN];

    inet_ntop(AF_INET, &_data, buffer, sizeof(buffer));

    return [NSString stringWithCString:buffer encoding:NSASCIIStringEncoding];
}

- (NSString *)longDescription
{
    return [NSString stringWithFormat:@"%03u.%03u.%03u.%03u",
            _data.u8[0],
            _data.u8[1],
            _data.u8[2],
            _data.u8[3]
            ];
}

- (IP4Address *)v4Address
{
    return self;
}

- (IP6Address *)v6Address
{
    return [[IP6Address alloc] initWithWords:0x00, 0x00, 0x00, 0x00, 0x00, 0xffff, ntohs(_data.u16[0]), ntohs(_data.u16[1])];
}

#pragma mark Properties

- (const void *)in_addr
{
    return &_data;
}

- (BOOL)isBroadcast
{
    return (_data.u32 == 0xffffffff)
        ? YES
        : NO;
}

- (NSUInteger)version
{
    return 4;
}

#pragma mark Comparison

- (NSComparisonResult)compare:(IPAddress *)address
{
    if ([[address class] isSubclassOfClass:[IP4Address class]]) {
        if (self != address) {
            IP4Address *v4address = (IP4Address *)address;

            for (NSUInteger i = 0; i < 4; i += 1) {
                if (_data.u8[i] > v4address->_data.u8[i]) {
                    return NSOrderedDescending;
                }

                if (_data.u8[i] < v4address->_data.u8[i]) {
                    return NSOrderedAscending;
                }
            }
        }
    }

    return NSOrderedSame;
}

- (BOOL)isEqualToIP:(IPAddress *)address
{
    if ([[address class] isSubclassOfClass:[IP4Address class]]) {
        if (self == address) {
            return YES;
        }
        else {
            IP4Address *v4address = (IP4Address *)address;

            return (0 == memcmp(&_data, &v4address->_data, sizeof(_data))) ? YES : NO;
        }
    }

    return NO;
}

@end
