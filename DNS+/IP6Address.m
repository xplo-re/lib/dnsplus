// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// IPv6 address.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>

@implementation IP6Address {
    // IP address words in network order (big endian).
    IP6AddressData _data;
}

#pragma mark Initialisation

- (id)initWithData:(void *)data length:(NSUInteger)length;
{
    self = [super init];

    if (nil != self) {
        memcpy(&_data, data, MIN(length, sizeof(_data)));
    }

    return self;
}

- (id)initWithWords:(unsigned)word0, ...
{
    self = [super init];

    if (nil != self) {
        va_list args;

        va_start(args, word0);

        _data.u16[0] = htons(word0);

        for (NSUInteger i = 1; i < 8; i += 1) {
            // Data is stored in big-endian network order, hence transform
            // as required.
            _data.u16[i] = htons(va_arg(args, unsigned));
        }

        va_end(args);
    }

    return self;
}

- (id)initWithString:(NSString *)address
{
    self = [super init];

    if (nil != self) {
        if (inet_pton(AF_INET6, address.UTF8String, &_data) <= 0) {
            return nil;
        }
    }

    return self;
}

+ (id)addressWithString:(NSString *)address
{
    return [[IP6Address alloc] initWithString:address];
}

+ (id)loopbackAddress
{
    return [[self alloc] initWithWords:0, 0, 0, 0, 0, 0, 0, 1];
}

+ (id)nodeLocalNodesAddress
{
    return [[self alloc] initWithWords:0xff01, 0, 0, 0, 0, 0, 0, 0x01];
}

+ (id)nodeLocalRoutersAddress
{
    return [[self alloc] initWithWords:0xff01, 0, 0, 0, 0, 0, 0, 0x02];
}

+ (id)linkLocalNodesAddress
{
    return [[self alloc] initWithWords:0xff02, 0, 0, 0, 0, 0, 0, 0x01];
}

+ (id)linkLocalRoutersAddress
{
    return [[self alloc] initWithWords:0xff02, 0, 0, 0, 0, 0, 0, 0x02];
}

+ (id)linkLocalMLDv2RoutersAddress
{
    return [[self alloc] initWithWords:0xff02, 0, 0, 0, 0, 0, 0, 0x16];
}

+ (id)linkLocalDHCPAgentsAddress
{
    return [[self alloc] initWithWords:0xff02, 0, 0, 0, 0, 0, 1, 0x02];
}

+ (id)siteLocalRoutersAddress
{
    return [[self alloc] initWithWords:0xff05, 0, 0, 0, 0, 0, 0, 0x02];
}

+ (id)siteLocalDHCPServersAddress
{
    return [[self alloc] initWithWords:0xff05, 0, 0, 0, 0, 0, 1, 0x03];
}

#pragma mark Conversion

- (NSString *)description
{
    char buffer[INET6_ADDRSTRLEN];

    inet_ntop(AF_INET6, &_data, buffer, sizeof(buffer));

    return [NSString stringWithCString:buffer encoding:NSASCIIStringEncoding];
}

- (NSString *)longDescription
{
    return [NSString stringWithFormat:@"%04x:%04x:%04x:%04x:%04x:%04x:%04x:%04x",
            ntohs(_data.u16[0]),
            ntohs(_data.u16[1]),
            ntohs(_data.u16[2]),
            ntohs(_data.u16[3]),
            ntohs(_data.u16[4]),
            ntohs(_data.u16[5]),
            ntohs(_data.u16[6]),
            ntohs(_data.u16[7])
            ];
}

- (IP4Address *)v4Address
{
    if (self.v4Mapped || self.v4Compatible) {
        return [[IP4Address alloc] initWithOctets:_data.u8[12], _data.u8[13], _data.u8[14], _data.u8[15]];
    }

    return nil;
}

- (IP6Address *)v6Address
{
    return self;
}

#pragma mark Properties

- (const void *)in_addr
{
    return &_data;
}

- (BOOL)isLinkLocal
{
    return (_data.u8[0] == 0xfe && (_data.u8[1] & 0xc0) == 0x80)
        ? YES
        : NO;
}

- (BOOL)isLoopback
{
    return (_data.u32[0] == 0 &&
            _data.u32[1] == 0 &&
            _data.u32[2] == 0 &&
            _data.u32[3] == htonl(1))
        ? YES
        : NO;
}

- (BOOL)isMulticast
{
    return (_data.u8[0] == 0xff)
        ? YES
        : NO;
}

- (BOOL)isSiteLocal
{
    return (_data.u8[0] == 0xfe && (_data.u8[1] & 0xc0) == 0xc0)
        ? YES
        : NO;
}

- (BOOL)isUniqueLocal
{
    return (_data.u8[0] == 0xfc || _data.u8[0] == 0xfd)
        ? YES
        : NO;
}

- (BOOL)isUnspecified
{
    return (_data.u32[0] == 0 &&
            _data.u32[1] == 0 &&
            _data.u32[2] == 0 &&
            _data.u32[3] == 0)
        ? YES
        : NO;
}

- (BOOL)isV4Compatible
{
    return (_data.u32[0] == 0 &&
            _data.u32[1] == 0 &&
            _data.u32[2] == 0 &&
            _data.u32[3] != 0 &&
            _data.u32[3] != htonl(1))
        ? YES
        : NO;
}

- (BOOL)isV4Mapped
{
    return (_data.u32[0] == 0 &&
            _data.u32[1] == 0 &&
            _data.u32[2] == htonl(0x0000ffff))
        ? YES
        : NO;
}

- (NSUInteger)version
{
    return 6;
}

#pragma mark Comparison

- (NSComparisonResult)compare:(IPAddress *)address
{
    if ([[address class] isSubclassOfClass:[IP6Address class]]) {
        if (self != address) {
            IP6Address *v6address = (IP6Address *)address;

            for (NSUInteger i = 0; i < 16; i += 1) {
                if (_data.u8[i] > v6address->_data.u8[i]) {
                    return NSOrderedDescending;
                }

                if (_data.u8[i] < v6address->_data.u8[i]) {
                    return NSOrderedAscending;
                }
            }
        }
    }
    
    return NSOrderedSame;
}

- (BOOL)isEqualToIP:(IPAddress *)address
{
    if ([[address class] isSubclassOfClass:[IP6Address class]]) {
        if (self == address) {
            return YES;
        }
        else {
            IP6Address *v6address = (IP6Address *)address;

            return (0 == memcmp(&_data, &v6address->_data, sizeof(_data))) ? YES : NO;
        }
    }

    return NO;
}

@end
