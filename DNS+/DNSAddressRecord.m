// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// DNS address (A) record interface.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/DNS+.h>

@implementation DNSAddressRecord

#pragma mark Record Type

+ (void)load
{
    [self register:self];
}

#pragma mark Initialisation

- (id)initWithHost:(DNSHost *)host timeToLive:(NSInteger)ttl address:(IPAddress *)address
{
    self = [super initWithHost:host timeToLive:ttl];

    if (nil != self) {
        _address = address;
    }

    return self;
}

- (id)initWithHost:(DNSHost *)host data:(void *)data
{
    self = [super initWithHost:host data:data];

    if (nil != self) {
        dns_resource_record_t *rr = (dns_resource_record_t *)data;

        _address = [IPAddress addressWithData:&rr->data.A->addr length:sizeof(rr->data.A->addr)];
    }

    return self;
}

#pragma mark Description

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ %@", [super description], self.address];
}

#pragma mark Record Type

+ (NSString *)type
{
    return kDNSAddressRecordType;
}

+ (NSUInteger)typeID
{
    return kDNSServiceType_A;
}

#pragma mark Reflection

- (NSDictionary *)properties
{
    NSMutableDictionary *properties = [NSMutableDictionary dictionaryWithDictionary:super.properties];

    properties[@"address"] = self.address;

    return [properties copy];
}

@end
