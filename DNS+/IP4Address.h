// -*- Mode: Objective-C; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
//////////////////////////////////////////////////////////////////////////////
// DNS Framework
//
// IPv4 address.
//
// Copyright(C) 2014, xplo.re IT Services, Michael Maier.
// All rights reserved.
//
// History:
// 1.0 Initial version. [tk]
//

#import <DNS+/IPAddress.h>

#pragma mark Support Structures

typedef struct _IP4AddressData {
    union {
        uint8_t u8[4];
        uint16_t u16[2];
        uint32_t u32;
    };
} IP4AddressData;

#pragma mark -

@interface IP4Address : IPAddress

#pragma mark Initialisation

/**
 * Initialises an IPv4 address with 4 provided octets.
 * @param octet0
 * First of four expected octets.
 */
- (id)initWithOctets:(unsigned)octet0, ...;

/**
 * Yields the IPv4 broadcast address 255.255.255.255.
 */
+ (id)broadcastAddress;

#pragma mark Properties

/**
 * @p YES, if receiver is the IPv4 broadcast address (255.255.255.255),
 * otherwise @p NO.
 */
@property (nonatomic, readonly, getter=isBroadcast) BOOL broadcast;

@end
